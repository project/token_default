<?php

namespace Drupal\token_default\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContentTypeForm.
 *
 * @package Drupal\section_node\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * A safety net recursive limit.
   */
  const DEFAULT_RECURSIVE_LIMIT = 5;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'token_default.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'token_default_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('token_default.settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable token defaults'),
      '#description' => $this->t('Switch on/off the token defaults functionality'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['recursive_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Allowed number of recursive replacements'),
      '#description' => $this->t('Prevent endless loop in case if we are using other tokens as replacement.'),
      '#default_value' => $config->get('recursive_limit') ?? self::DEFAULT_RECURSIVE_LIMIT,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('token_default.settings');

    $config->set('enabled', $form_state->getValue('enabled'));
    $config->set('recursive_limit', $form_state->getValue('recursive_limit'));

    $config->save();
  }

}
